﻿namespace API_1.DMO
{
    public class Details
    {
        public float price { get; set; }
        public string description { get; set; }
        public string color { get; set; }

        public Details(float price, string description, string color)
        {
            this.price = price;
            this.description = description;
            this.color = color;
        }
    }
}