﻿namespace API_1.DMO
{
    public class Order
    {
        public DateTime createdAt { get; set; }
        public int id { get; set; }
        public int customerId { get; set; }
        public string? title { get; set; }

        public Product[] productList { get; set; }

        public Order(DateTime createdAt, int id, int customerId, string? title, Product[] productList)
        {
            this.createdAt = createdAt;
            this.id = id;
            this.customerId = customerId;
            this.title = title;
            this.productList = productList;
        }
    }
}