﻿using API_1.DMO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace API_1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProductsController : ControllerBase
    {
        [HttpGet()]
        public List<Product> GetAllProducts()
        {
            List<Product> ProductList = new List<Product>();

            var webRequest = new HttpRequestMessage(HttpMethod.Get, "https://63f7a7dfe40e087c95939883.mockapi.io/api/v1/products/");

            var httpClient = new HttpClient();

            var response = httpClient.Send(webRequest);

            if (!response.IsSuccessStatusCode)
            {
                return ProductList;
            }

            using var reader = new StreamReader(response.Content.ReadAsStream());
            string responseJson = reader.ReadToEnd();

            ProductList = JsonConvert.DeserializeObject<List<Product>>(responseJson);

            return ProductList;

        }

        [HttpGet("{id}")]
        public Product? GetProduct(int id)
        {
            var webRequest = new HttpRequestMessage(HttpMethod.Get, ("https://63f7a7dfe40e087c95939883.mockapi.io/api/v1/products/" + id.ToString()));

            var httpClient = new HttpClient();

            var response = httpClient.Send(webRequest);

            if (!response.IsSuccessStatusCode)
            {
                return null;
            }

            using var reader = new StreamReader(response.Content.ReadAsStream());
            string responseJson = reader.ReadToEnd();

            Product product = JsonConvert.DeserializeObject<Product>(responseJson);

            return product;
        }
    }
}
