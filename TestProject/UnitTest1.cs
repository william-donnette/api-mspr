
using API_1.DMO;
using Microsoft.AspNetCore.Http.HttpResults;
using static System.Runtime.InteropServices.JavaScript.JSType;
using System.Net;
using System.Xml.Linq;

namespace TestProject
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }
        [Test]
        public void Test1()
        {

            Newtonsoft.Json.Linq.JObject company = new Newtonsoft.Json.Linq.JObject();
            company.Add("companyName", "bonjour");

            Customer customer = new Customer(
            new DateTime(), 
            new string(""), 
            new string(""),
            new string(""),
            new string(""), 
            new int(),
            new string(""), 
            new Address("", ""), 
            new Profile("",""),
            company, 
            new Order[0]
            );

            string expectedResult = "bonjour";
            string constructorResult = customer.company.companyName;

            Assert.AreEqual(constructorResult, expectedResult);

        }
    }
}